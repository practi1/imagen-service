package com.practica.imagenservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.practica.commonsmodule.error.config.ManejadorErrores;

@SpringBootApplication
@Import(ManejadorErrores.class)
public class ImagenServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImagenServiceApplication.class, args);
	}

}
