package com.practica.imagenservice.config;

import java.util.Arrays;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
public class SpringFoxConfig {
	
	public static final String AUTHORIZATION_HEADER = "Authorization";

	//todos los controller
	/*@Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(RequestHandlerSelectors.any())              
          .paths(PathSelectors.any())                   
          .build();                                           
    }*/

	//uno por paquete
	/*@Bean
    public Docket swaggerModuleLoginApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                //.groupName("imagen")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.practica.imagenservice"))              
                .paths(PathSelectors.any()) 
                .build();
    }*/
	
	//con token
    @Bean
    public Docket api() { 
        return new Docket(DocumentationType.SWAGGER_2)  
	      .securityContexts(Arrays.asList(securityContext()))
	      .securitySchemes(Arrays.asList(apiKey()))
          .select()
          .apis(RequestHandlerSelectors.basePackage("com.practica.imagenservice"))        
          .paths(PathSelectors.any())
          .build();                                           
    }

	//activa la solicitud del token
    private ApiKey apiKey() {
      return new ApiKey("JWT", AUTHORIZATION_HEADER, "header");
    }

    private SecurityContext securityContext() {
      return SecurityContext.builder()
          .securityReferences(defaultAuth())
          .build();
    }

    List<SecurityReference> defaultAuth() {
      var authorizationScope = new AuthorizationScope("global", "accessEverything");
      var authorizationScopes = new AuthorizationScope[1];
      authorizationScopes[0] = authorizationScope;
      return Arrays.asList(new SecurityReference("JWT", authorizationScopes));
    }
	
}
