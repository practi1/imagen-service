package com.practica.imagenservice.controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.practica.commonsmodule.error.FaltaArgumento;
import com.practica.commonsmodule.utilis.Convert;
import com.practica.imagenservice.dto.entrada.IIdImagenDto;
import com.practica.imagenservice.dto.entrada.IImagenDto;
import com.practica.imagenservice.dto.salida.OImagenDto;
import com.practica.imagenservice.service.ImagenService;

//feign:cliente-service
@RestController
@RequestMapping("/imagenes")
@RolesAllowed("admin")
public class ImagenController {

    @Autowired
    ImagenService imagenService;
    
    @GetMapping("/all")
    public ResponseEntity<List<OImagenDto>> list() {
    	return new ResponseEntity<>(imagenService.findAll(), HttpStatus.OK);
    }
    
    @GetMapping
    public ResponseEntity<Page<OImagenDto>> getAllPages(
                        @RequestParam(defaultValue = "0") Integer pageNo, 
                        @RequestParam(defaultValue = "10") Integer pageSize,
                        @RequestParam(defaultValue = "id") String sortBy) {
        return new ResponseEntity<>(imagenService.getAllPages(pageNo, pageSize, sortBy), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<OImagenDto> get(@PathVariable String id) {
		return new ResponseEntity<>(imagenService.findOne(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<OImagenDto> add(@Valid @RequestBody IImagenDto iImagen, BindingResult bindingResult) {
    	if (bindingResult.hasErrors()) {
    		throw new FaltaArgumento(Convert.bindigResultToString(bindingResult));
    	}
    	return new ResponseEntity<>(imagenService.save(iImagen), HttpStatus.CREATED);
    }

    @PostMapping("/file")
    public ResponseEntity<OImagenDto> addFile(@RequestPart MultipartFile imagenfile) throws IOException {
    	return new ResponseEntity<>(imagenService.saveFile(imagenfile), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<OImagenDto> update(@Valid @RequestBody IImagenDto iImagen, BindingResult bindingResult) {
    	if (bindingResult.hasErrors()) {
    		throw new FaltaArgumento(Convert.bindigResultToString(bindingResult));
    	}
    	return new ResponseEntity<>(imagenService.update(iImagen), HttpStatus.OK);
    }

    @PutMapping("/file")
    public ResponseEntity<OImagenDto> updateFile(@RequestParam String id, @RequestPart MultipartFile imagenfile) throws IOException {
    	return new ResponseEntity<>(imagenService.updateFile(id, imagenfile), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<OImagenDto> delete(@PathVariable String id) {
    	return new ResponseEntity<>(imagenService.deleteOne(id), HttpStatus.OK);
    }
    
    //endpoint requeridos

    @PostMapping("/ids")
    public ResponseEntity<List<OImagenDto>> listById(@Valid @RequestBody IIdImagenDto listId, BindingResult bindingResult) {
    	if (bindingResult.hasErrors()) {
    		throw new FaltaArgumento(Convert.bindigResultToString(bindingResult));
    	}
    	return new ResponseEntity<>(imagenService.findAllById(listId), HttpStatus.OK);
    }

}
