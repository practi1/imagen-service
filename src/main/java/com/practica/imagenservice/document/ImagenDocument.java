package com.practica.imagenservice.document;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;
import org.springframework.data.mongodb.core.mapping.MongoId;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data //@ToString, @EqualsAndHashCode, @Getter/@Setter y @RequiredArgsConstructor
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "imagen")
public class ImagenDocument {
	@MongoId(value = FieldType.OBJECT_ID)
    private String id;
	
	@Field
	private String titulo;
	
	@Field
	private String mimeType;
	
	@Field
	private String imagen64;

}
