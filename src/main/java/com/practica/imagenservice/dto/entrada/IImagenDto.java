package com.practica.imagenservice.dto.entrada;

import java.io.Serializable;

import javax.validation.constraints.NotEmpty;

import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data //@ToString, @EqualsAndHashCode, @Getter/@Setter y @RequiredArgsConstructor
@NoArgsConstructor
@Builder
public class IImagenDto implements Serializable {
	
	private static final long serialVersionUID = 1L;	
    private String id;
	@NotEmpty(message = "Titulo no puede ser vacio o nulo")
	private String titulo;
	@NotEmpty(message = "Tipo no puede ser vacio o nulo")
	private String mimeType;
	@NotEmpty(message = "Imagen no puede ser vacio o nulo")
	private String imagen64;
	
	public IImagenDto(String id, String titulo, String mimeType, String imagen64) {
		if ((id != null) && (id.isBlank())) {
			this.id = null;
		} else {
			this.id = id;
		}
		this.titulo = titulo;
		this.mimeType = mimeType;
		this.imagen64 = imagen64;
	}

	public void setId(String id) {
		if ((id != null) && (id.isBlank())) {
			this.id = null;
		} else {
			this.id = id;
		}
	}

}
