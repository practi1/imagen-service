package com.practica.imagenservice.repository;

import java.util.Optional;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.practica.imagenservice.document.ImagenDocument;

@Repository
public interface ImagenRepository extends MongoRepository<ImagenDocument, String> {
	Optional<ImagenDocument> findTopByOrderByIdDesc();

}
