package com.practica.imagenservice.service;

import java.io.IOException;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import com.practica.imagenservice.dto.entrada.IIdImagenDto;
import com.practica.imagenservice.dto.entrada.IImagenDto;
import com.practica.imagenservice.dto.salida.OImagenDto;

public interface ImagenService {
	public List<OImagenDto> findAll();
	public Page<OImagenDto> getAllPages(Integer pageNo, Integer pageSize, String sortBy);
	public OImagenDto findOne(String id);
	public OImagenDto save(IImagenDto iImagen);
	public OImagenDto saveFile(MultipartFile imagenfile) throws IOException;
	public OImagenDto update(IImagenDto iImagen);
	public OImagenDto updateFile(String id, MultipartFile imagenfile) throws IOException;
	public OImagenDto deleteOne(String id);
    //endpoint requeridos
	public List<OImagenDto> findAllById(IIdImagenDto listId);

}
