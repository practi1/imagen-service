package com.practica.imagenservice.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.practica.commonsmodule.error.Encontrado;
import com.practica.commonsmodule.error.FaltaArgumento;
import com.practica.commonsmodule.error.NoEncontrado;
import com.practica.commonsmodule.utilis.FormatString;
import com.practica.imagenservice.document.ImagenDocument;
import com.practica.imagenservice.dto.entrada.IIdImagenDto;
import com.practica.imagenservice.dto.entrada.IImagenDto;
import com.practica.imagenservice.dto.salida.OImagenDto;
import com.practica.imagenservice.repository.ImagenRepository;
import com.practica.imagenservice.service.ImagenService;
import com.practica.imagenservice.util.ImagenConverter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ImagenServiceImpl implements ImagenService {
	
	private static final String RECURSO = "imagen";

    @Autowired
    private ImagenRepository imagenRepository;

    private ImagenDocument imagenDocument;

	@Override
	public List<OImagenDto> findAll() {
		List<OImagenDto> nImagenesDto = new ArrayList<>();
		List<ImagenDocument> nImagenes = imagenRepository.findAll();
		for(ImagenDocument imagen : nImagenes) {
			nImagenesDto.add(ImagenConverter.convertImagenDocumentToOImagenDto(imagen));
		}
		log.info("se consulta listado de imagenes");
        return nImagenesDto;
	}

	@Override
	public Page<OImagenDto> getAllPages(Integer pageNo, Integer pageSize, String sortBy){
		List<OImagenDto> nImagenesDto = new ArrayList<>();
        Pageable paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy)); 
        Page<ImagenDocument> nImagenes = imagenRepository.findAll(paging);
		for(ImagenDocument imagen : nImagenes) {
			nImagenesDto.add(ImagenConverter.convertImagenDocumentToOImagenDto(imagen));
		}
		log.info("se consulta paginado de imagenes");
		return new PageImpl<>(nImagenesDto, nImagenes.getPageable(), nImagenes.getTotalElements());
	}

	@Override
    public OImagenDto findOne(String id) {
		try {
			new ObjectId(id);
		} catch(Exception e) {
			throw new FaltaArgumento(FormatString.noEsTipoString("ID", "ObjectId", id));
		}
		Optional<ImagenDocument> nImagenOpt = imagenRepository.findById(id);
		if (nImagenOpt.isEmpty()) {
			throw new NoEncontrado(FormatString.noExisteString(RECURSO, "id", id));
		}
		log.info("se consulta imagen {}", id);
		return ImagenConverter.convertImagenDocumentToOImagenDto(nImagenOpt.get());
    }

	@Override
	public OImagenDto save(IImagenDto iImagen) {	
		if ((iImagen.getId() != null) && (!iImagen.getId().isBlank())) {
			throw new Encontrado(FormatString.seEnvioString("id"));
		}
		ImagenDocument nImagen = ImagenConverter.convertIImagenDtoToImagenDocument(iImagen);
		nImagen = imagenRepository.save(nImagen);
		log.info("se crea imagen con el id: {}",  nImagen.getId());
        return ImagenConverter.convertImagenDocumentToOImagenDto(nImagen);
	}

	@Override
    public OImagenDto saveFile(MultipartFile imagenfile) throws IOException {
		if (imagenDocument == null) {
			imagenDocument = ImagenDocument.builder().build();
		}
		imagenDocument.setId(null);
		imagenDocument.setTitulo(imagenfile.getOriginalFilename());
		imagenDocument.setMimeType(imagenfile.getContentType());
		imagenDocument.setImagen64(Base64.getEncoder().encodeToString(imagenfile.getBytes()));
        ImagenDocument imagen = imagenRepository.save(imagenDocument);      
		log.info("se crea imagen con el id: {}",  imagen.getId());   
        return ImagenConverter.convertImagenDocumentToOImagenDto(imagen);
    }

	@Override
	public OImagenDto update(IImagenDto iImagen) {
		if (imagenRepository.findById(iImagen.getId()).isEmpty()) {
			throw new NoEncontrado(FormatString.noExisteString(RECURSO, "id", iImagen.getId()));
		}
		ImagenDocument nImagen = ImagenConverter.convertIImagenDtoToImagenDocument(iImagen);
		nImagen = imagenRepository.save(nImagen);
		log.info("se actualiza imagen con el id: {}", nImagen.getId());
        return ImagenConverter.convertImagenDocumentToOImagenDto(nImagen);
	}

	@Override
	public OImagenDto updateFile(String id, MultipartFile imagenfile) throws IOException {   
		if (imagenRepository.findById(id).isEmpty()) {
			throw new NoEncontrado(FormatString.noExisteString(RECURSO, "id", id));
		}
		if (imagenDocument == null) {
			imagenDocument = ImagenDocument.builder().build();
		}
		imagenDocument.setId(id);
		imagenDocument.setTitulo(imagenfile.getOriginalFilename());
		imagenDocument.setMimeType(imagenfile.getContentType());
		imagenDocument.setImagen64(Base64.getEncoder().encodeToString(imagenfile.getBytes()));
        ImagenDocument imagen = imagenRepository.save(imagenDocument);    
		log.info("se actualiza imagen con el id: {}", imagen.getId());     
        return ImagenConverter.convertImagenDocumentToOImagenDto(imagen);
	}

	@Override
	public OImagenDto deleteOne(String id) {
		Optional<ImagenDocument> nImagenOpt = imagenRepository.findById(id);
		if (nImagenOpt.isEmpty()) {
			throw new NoEncontrado(FormatString.noExisteString(RECURSO, "id", id));
		}
		imagenRepository.deleteById(id);
		log.info("se elimina imagen con el id: {}", id);
		return ImagenConverter.convertImagenDocumentToOImagenDto(nImagenOpt.get());
	}
    
    //endpoint requeridos

	@Override
	public List<OImagenDto> findAllById(IIdImagenDto listId) {
		List<OImagenDto> nImagenesDto = new ArrayList<>();
		Iterable<ImagenDocument> nImagenes = imagenRepository.findAllById(listId.getIds());
		for(ImagenDocument imagen : nImagenes) {
			nImagenesDto.add(ImagenConverter.convertImagenDocumentToOImagenDto(imagen));
		}
		log.info("se consulta listado de imagenes según Id");
        return nImagenesDto;
	}

}
