package com.practica.imagenservice.util;

import com.practica.imagenservice.document.ImagenDocument;
import com.practica.imagenservice.dto.entrada.IImagenDto;
import com.practica.imagenservice.dto.salida.OImagenDto;

public class ImagenConverter {
	
	private ImagenConverter() {
		throw new IllegalStateException();
	}
	
	public static IImagenDto convertImagenDocumentToIImagenDto(ImagenDocument imagenDocument) {
		return IImagenDto.builder()
				.id(imagenDocument.getId())
				.titulo(imagenDocument.getTitulo())
				.mimeType(imagenDocument.getMimeType())
				.imagen64(imagenDocument.getImagen64())
				.build();
	}
	
	public static OImagenDto convertImagenDocumentToOImagenDto(ImagenDocument imagenDocument) {
		return OImagenDto.builder()
				.id(imagenDocument.getId())
				.titulo(imagenDocument.getTitulo())
				.mimeType(imagenDocument.getMimeType())
				.imagen64(imagenDocument.getImagen64())
				.build();
	}

	public static ImagenDocument convertIImagenDtoToImagenDocument(IImagenDto iImagenDto) {
		return ImagenDocument.builder()
				.id(iImagenDto.getId())
				.titulo(iImagenDto.getTitulo())
				.mimeType(iImagenDto.getMimeType())
				.imagen64(iImagenDto.getImagen64())
				.build();
	}
	
	public static ImagenDocument convertOImagenDtoToImagenDocument(OImagenDto oImagenDto) {
		return ImagenDocument.builder()
				.id(oImagenDto.getId())
				.titulo(oImagenDto.getTitulo())
				.mimeType(oImagenDto.getMimeType())
				.imagen64(oImagenDto.getImagen64())
				.build();
	}

}
