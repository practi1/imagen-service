package com.practica.imagenservice;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.practica.imagenservice.controller.ImagenController;

@SpringBootTest
class ImagenServiceApplicationTests {
	
	@Autowired
	 private ImagenController imagenController;

	@Test
	void contextLoads() {
		Assertions.assertNotNull(imagenController);
	}

}
