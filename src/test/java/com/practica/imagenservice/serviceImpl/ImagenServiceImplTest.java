package com.practica.imagenservice.serviceImpl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.multipart.MultipartFile;

import com.practica.commonsmodule.error.Encontrado;
import com.practica.commonsmodule.error.NoEncontrado;
import com.practica.imagenservice.document.ImagenDocument;
import com.practica.imagenservice.dto.entrada.IImagenDto;
import com.practica.imagenservice.dto.salida.OImagenDto;
import com.practica.imagenservice.repository.ImagenRepository;
import com.practica.imagenservice.service.impl.ImagenServiceImpl;
import com.practica.imagenservice.util.ImagenConverter;

//pruebas unitarias
//todas las pruebas son simultaneas
@ExtendWith(MockitoExtension.class)
class ImagenServiceImplTest {

	@InjectMocks
	ImagenServiceImpl imagenServiceImpl;
	
	@Mock
	ImagenRepository imagenRepository;
	
	@Mock
	ImagenDocument imagenDocument;
	
	@Mock
	IImagenDto iImagenDto;
	
	@Mock
	OImagenDto oImagenDto;
	
	private static final String ID = "60d61ecb655c7013c8a9a5b2";
	private static final String TITULO = "my titulo";
	private static final String MIMETYPE = "image/jpeg";
	private static final String IMAGEN64 = "/9j/4AAQSkZJRgABAQIAJQAlAAD/2wBDAAYEBQYFBAYGBQYHBwYIC"
			+ "hAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/wAALCAHDAmwBAREA/8QAH"
			+ "QABAAIDAQEBAQAAAAAAAAAAAAgJBQYHBAMBAv/EAD4QAQAAAgUHCAkFAAIDAQAAAAABAwIEBQYHCDdWdZSz0hEXG"
			+ "DZRVXTRFRYhNZKTsbLDEhMUMXNBYTJxkSL/2gAIAQEAAD8AlSAAAAKysQOvl5NZ1ne0mAABZNhBmquhqqrbqi24A"
			+ "AAAAAAAAABCfLQzq1HVUrezXAwATJyJeodvaz/FQSLAAAAAAAAAVlYgdfLyazrO9pMAACybCDNVdDVVW3VFtwAAA"
			+ "AAAAAAACE+WhnVqOqpW9muBgAmTkS9Q7e1n+KgkWAAAAAAAAArKxA6+Xk1nWd7SYAAFk2EGaq6GqqtuqLbgAAAAA"
			+ "AAAAAEJ8tDOrUdVSt7NcDABMnIl6h29rP8AFQSLAAAAAAAAAVlYgdfLyazrO9pMAACybCDNVdDVVW3VFtwAAAAAA"
			+ "AAAACE+WhnVqOqpW9muBgAmTkS9Q7e1n+KgkWAAAAAAAAArKxA6+Xk1nWd7SYAAFk2EGaq6GqqtuqLbgAAAAAAAA"
			+ "AAEJ8tDOrUdVSt7NcDABMnIl6h29rP8VBIsAAAAAAAABWViB18vJrOs72kwAALJsIM1V0NVVbdUW3AAAAAAAAAAA"
			+ "IT5aGdWo6qlb2a4GACZORL1Dt7Wf4qCRYAAAAAAAACsrEDr5eTWVZ3tJgAAWTYQZqroaqq26otuAAAAAAAAAAAQn"
			+ "y0M6tR1VK3s1wMAEyciXqHb2svxUEiwAAAAAAAAHhp2PZlOnSp07OqdKlSjyxjGRRjGMe3+n56Fsruyo/IoeR6Fs"
			+ "ruyo/IoeR6Fsruyo/IoeR6Fsruyo/IoeR6Fsruyo/IoeR6Fsruyo/IoeSvTFa06/VcTb1yKtXq1JkS7TrFChLlza"
			+ "VGjQowmUuSEIQjyQh/01X01avede+fT8z01avede+fT8z01avede+fT8z01avede+fT8z01avede+fT8z01avede"
			+ "+fT81hOBU2ZOwhupMnU6cyZSqVGNKlTjyxjHlj/AHGLewAAAAABCLK2tKvVXF6dLq1drMmX/CkR/TLm0qMOXkj/A"
			+ "MQi4x6atXvOvfPp+Z6atXvOvfPp+Z6atXvOvfPp+Z6atXvOvfPp+Z6atXvOvfPp+Z6atXvOvfPp+aYuSHIk2rhlX"
			+ "Z9qSpddnwtObQhMrNGEylCj+3K9nLS5Y8ntj7P+3cPQtld2VH5FDyPQtld2VH5FDyPQtld2VH5FDyPQtld2VH5FD"
			+ "yPQtld2VH5FDyPQtld2VH5FDyemq1Sr1ShGhVKvJkUYx5YwlUIUYRj2+x9gAAAAAAAAAABW1i/nVvfrWs72k1AAF"
			+ "i+AmZy6XgaP1i30AAAAAAQXyv8APHO8DI+kXEwATZyL81Ve1rN3Up3sAAAAAAAAAAAAAFbWL+dW9+tazvaTUAAWL"
			+ "4CZnLpeBo/WLfQAAAAABBfK/wA8c7wMj6RcTABNnIvzVV7Ws3dSnewAAAAAAAAAAAAAVtYv51b361rO9pNQABYvg"
			+ "Jmcul4Gj9Yt9AAAAAAEF8r/ADxzvAyPpFxMAE2ci/NVXtazd1Kd7AAAAAAAAAAAAABW1i/nVvfrWs72k1AAFi+Am"
			+ "Zy6XgaP1i30AAAAAAQXyv8APHO8DI+kXEwATZyL81Ve1rN3Up3sAAAAAAAAAAAAAFbWL+dW9+tazvaTUAAWL4CZn"
			+ "LpeBo/WLfQAAAAABBfK/wA8c7wMj6RcTABNnIvzVV7Ws3dSnewAAAAAAAAAAAAAVtYv51b361rO9pNQABYvgLmcu"
			+ "l4Gh9Yt9AAAAAAEGMr/ADxzvAyPpFxIAE2ci/NVXtazd1Kd7AAAAAAAAAAAFc9+r4XmkX3vDKk3itmXKl2jWKNCh"
			+ "Qr02EKMITaUIQhD9XsgwfrrenSW29vm8R663p0ltvb5vEeut6dJbb2+bxHrrenSW29vm8R663p0ltvb5vEeut6dJ"
			+ "bb2+bxMJWZ86tViZPrM2ZOnzaUadOZMpRpUqdKP9xjGPtjF8gAZqqXrvFUqtLq1Tt61qvV5cP00JUquTKFGjDshC"
			+ "EeSD7eut6dJbb2+bxHrrenSW29vm8R663p0ltvb5vEeut6dJbb2+bxHrrenSW29vm8R663p0ltvb5vEeut6dJbb2"
			+ "+bxHrrenSW29vm8R663p0ltvb5vEeut6dJbb2+bxHrrenSW29vm8R663p0ltvb5vEeut6dJbb2+bxHrrenSW29vm"
			+ "8R663p0ltvb5vEeut6dJbb2+bxHrrenSW29vm8R663p0ltvb5vEeut6dJbb2+bxHrrenSW29vm8TE2laNdtSsxrN"
			+ "p1ys1ysRhCj+7WJtKZS5If1DljGMXlABlLMvFbVlVeMiy7YtGpSI0v1xl1atU5dGNLt5KMYQ5fZD/49nrrenSW29"
			+ "vm8R663p0ltvb5vEeut6dJbb2+bxHrrenSW29vm8R663p0ltvb5vEeut6dJbb2+bxJbZHNrWja9yLbm2rX63XptC"
			+ "0f00adZnUpsaMP2qEeSEaUY8kHfgAAAAAAAAFZWIHXy8ms6zvaTAAAAAAAAAAAAAAAAAmTkS9Q7e1n+KgkWAAAAA"
			+ "AAAArKxA6+Xk1nWd7SYAAAAAAAAAAAAAAAAEyciXqHb2s/xUEiwAAAAAAAAFZWIHXy8ms6zvaTAAAAAAAAAAAAAA"
			+ "AAAmTkS9Q7e1n+KgkWAAAAAAAAArKxA6+Xk1nWd7SYAAAAAAAAAAAAAAAAEyciXqHb2s/xUEiwAAAAAAAAFZWIHX"
			+ "y8ms6zvaTAAAAAAAAAAAAAAAAAmTkS9Q7e1n+KgkWAAAAAAAAArKxA6+Xk1nWd7SYAAAAAAAAAAAAAAAAEyciXqH"
			+ "b2s/wAVBIsAAAAAAFdFt4l33lW1X5cu9tu0KFCsTKNGjCvTIQhCFKPJCHteLnOv1pfb23TPM5zr9aX29t0zzOc6/"
			+ "Wl9vbdM8znOv1pfb23TPM5zr9aX29t0zzOc6/Wl9vbdM82qVmfNrVZm1iszKc2fNpxmTJlOPLSp0ox5YxjH/mMYv"
			+ "kACfmFuHdza/htdet1261iz61Ps2rzJs2ZU5dKlTpRl0YxjGMYe2MYto5sbi6IWDsMvyObG4uiFg7DL8jmxuLohY"
			+ "Owy/I5sbi6IWDsMvyObG4uiFg7DL8jmxuLohYOwy/JBDGmo1SzMVbzVKzqtKqtUk1ylQlSZNCFGhQhyQ9kIQ9kIN"
			+ "KAAAAAAEx8lu5V2LdwqlV22rv2XX63GuTqEZ1ZqtCZT5IRhyQ5Yw5eR13mxuLohYOwy/I5sbi6IWDsMvyObG4uiF"
			+ "g7DL8jmxuLohYOwy/I5sbi6IWDsMvyObG4uiFg7DL8kRcrKw7Ku/iTU6pYdnVSz6rSs2XMjKqsqjLoxpRmTIRjyQ"
			+ "/55IQ/+OKgAztgXuvFd2rTKvYNuWlZ0iZT/AHKcuq1ilLo0qXJycsYQj/fJCDJ851+tL7e26Z5nOdfrS+3tumeZz"
			+ "nX60vt7bpnmc51+tL7e26Z5nOdfrS+3tumeZznX60vt7bpnmk/kfXktq8Vi3kmW/atetKnJrEmjLpVqdSmRoQjRp"
			+ "csIcsfYkKAAAAKu7we/rS8TN+6LHgAAALJsIM1V0NVVbdUW3AArpx7zx3t8dS+kGggAAAAACdGSBmck+On/AFg7Y"
			+ "ACE+WhnVqOqpW9muBgAAAJdZD3uG9XiZH20kmgAAABV3eD39aXiZv3RY8AAABZNhBmquhqqrbqi24AFdOPeeO9vj"
			+ "qX0g0EAAAAAAToyQMzknx0/6wdsABCfLQzq1HVUrezXAwAAAEush73DerxMj7aSTQAAAAq7vB7+tLxM37oseAAAA"
			+ "smwgzVXQ1VVt1RbcACunHvPHe3x1L6QaCAAAAAAJ0ZIGZyT46f9YO2AAhPloZ1ajqqVvZrgYAAACXWQ97hvV4mR9"
			+ "tJJoAAAAVd3g9/Wl4mb90WPAAAAWTYQZqroaqq26otuABXTj3njvb46l9INBAAAAAAE6MkDM5J8dP8ArB2wAEJ8t"
			+ "DOrUdVSt7NcDAAAAS6yHvcN6vEyPtpJNAAAACru8Hv60vEzfuix4AAACybCDNVdDVVW3VFtwAK6ce88d7fHUvpBo"
			+ "IAAAAAAnRkgZnJPjp/1g7YACE+WhnVqOqpW9muBgAAAJdZD3uG9XiZH20kmgAAABV3eD39aXiZv3RY8AAABZNhBm"
			+ "quhqqrbqi24AFdOPeeO9vjqX0g0EAAAAAAToyQMzknx0/6wdsABCfLQzq1HVUrezXAwAAAEush73DerxMj7aSTQA"
			+ "CuvnlxD0stL4oeRzy4h6WWl8UPI55cQ9LLS+KHkc8uIellpfFDyOeXEPSy0vih5HPLiHpZaXxQ8jnlxD0stL4oeT"
			+ "Qp02nOnU5s2lGlMp0o0qVKP9xjH2xi/gAAABu9m4r36s2z6tUahea0JFUq0ujKkyqFKHJQoUYckIQ9n9QhB6eeXE"
			+ "PSy0vih5HPLiHpZaXxQ8jnlxD0stL4oeRzy4h6WWl8UPI55cQ9LLS+KHkc8uIellpfFDyaXa9pVy2LTrFoWnWKdZ"
			+ "rtYp/rmzqf/AJU6XbF4wAAAAABtl28Rb3XZsyFn2Db1dqNShTjThJlUoQo/qj/cf6ZTnlxD0stL4oeRzy4h6WWl8"
			+ "UPI55cQ9LLS+KHkc8uIellpfFDyOeXEPSy0vih5HPLiHpZaXxQ8mr3nvLbN6bQoV68NoT7QrdCXCVRmzo8sYUIRj"
			+ "GEP/XLGP/1hwAAAGx3VvveW6cmsSruWxWrOl1ilClNoyYwhCnGHshGPs/7Z3nlxD0stL4oeRzy4h6WWl8UPI55cQ"
			+ "9LLS+KHkc8uIellpfFDyOeXEPSy0vih5HPLiHpZaXxQ8jnlxD0stL4oeTn4AAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
			+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
			+ "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACfNj4FYcViyKjOnXalUpkyRQp0qX8mf7YxowjGP/m9nMLhro"
			+ "xK2qfxnMLhroxK2qfxnMLhroxK2qfxnMLhroxK2qfxnMLhroxK2qfxnMLhroxK2qfxoJXxqkioXutyp1SX+3VqvX"
			+ "p8mVQ5Yx/TQozKUIQ5Y+3+oQYcAAAAAAAAAAAAAAAEmsljDe6d9Lo2vXLzWRQr1ZkV79mXTpTplD9ND9ujHk5KNK"
			+ "EP7jF2vmFw10YlbVP4zmFw10YlbVP4zmFw10YlbVP4zmFw10YlbVP4zmFw10YlbVP4zmFw10YlbVP40ccqy493bk"
			+ "2vd+TdizaNQl1mRNpzqNGZTp/qjClRhCP8A+ox7YuEAAAAC0W7/ALhs3w0r7YPeACsrEDr5eTWdZ3tJgAAAAAAAA"
			+ "AAAAAAAATJyJeodvaz/ABUEiwARGy4ff11fDT/uooyAAAAC0W7/ALhs3w0r7YPeACsrEDr5eTWdZ3tJgAAAAAAAA"
			+ "AAAAAAAATJyJeodvaz/ABUEiwARGy4ff11fDT/uooyAAAAC0W7/ALhs3w0r7YPeACsrEDr5eTWdZ3tJgAAAAAAAA"
			+ "AAAAAAAATJyJeodvaz/ABUEiwARGy4ff11fDT/uooyAAAAC0W7/ALhs3w0r7YPeACsrEDr5eTWdZ3tJgAAAAAAAA"
			+ "AAAAAAAATJyJeodvaz/ABUEiwARGy4ff11fDT/uooyAAAAC0W7/ALhs3w0r7YPeACsrEDr5eTWdZ3tJgAAAAAAAA"
			+ "AAAAAAAATJyJeodvaz/ABUEiwARGy4ff11fDT/uooyAAAAC0W7/ALhs3w0r7YPeACsrEDr5eTWdZ3tJgAAAAAAAA"
			+ "AAAAAAAATJyJeodvaz/ABUEiwARGy4ff11fDT/uooyAAAAC0W7/ALhs3w0r7YPeACsrEDr5eTWdZ3tJgAATTw7wF"
			+ "uBbVwru2paFl1iZXK5UJE+dThXJtGFKnSoQjGPJClyQ9sWxdHLDbuis7dO4jo5Ybd0Vnbp3EdHLDbuis7dO4jo5Y"
			+ "bd0Vnbp3EdHLDbuis7dO4jo5Ybd0Vnbp3EdHLDbuis7dO4jo5Ybd0Vnbp3EdHLDbuis7dO4jo5Ybd0Vnbp3EdHLD"
			+ "buis7dO4jo5Ybd0Vnbp3EdHLDbuis7dO4jo5Ybd0Vnbp3EdHLDbuis7dO4jo5Ybd0Vnbp3EdHLDbuis7dO4jo5Yb"
			+ "d0Vnbp3EdHLDbuis7dO4jo5Ybd0Vnbp3EdHLDbuis7dO4jo5Ybd0Vnbp3EdHLDbuis7dO4jo5Ybd0Vnbp3EdHLDb"
			+ "uis7dO4jo5Ybd0Vnbp3EdHLDbuis7dO4jo5Ybd0Vnbp3EdHLDbuis7dO4jo5Ybd0Vnbp3EdHLDbuis7dO4jo5Ybd"
			+ "0Vnbp3EdHLDbuis7dO4jo5Ybd0Vnbp3EjHlK3MsS41/arZd26tTq9TmVCXPpUKc2lMjGnGnMhGPLSjGP9UYOTAAm"
			+ "TkS9Q7e1n+KgkWACI2XD7+ur4af91FGQAAAAWi3f9w2b4aV9sHvABWViB18vJrOs72kwAALJsIM1V0NVVbdUW3AA"
			+ "AAAAAAAAAIT5aGdWo6qlb2a4GACZORL1Dt7Wf4qCRYAIjZcPv66vhp/3UUZAAAABaLd/wBw2b4aV9sHvABWViB18"
			+ "vJrOs72kwAALJsIM1V0NVVbdUW3AAAAAAAAAAAIT5aGdWo6qlb2a4GACZORL1Dt7Wf4qCRYAIjZcPv66vhp/wB1F"
			+ "GQAAAAWi3f9w2b4aV9sHvABWViB18vJrOs72kwAALJsIM1V0NVVbdUW3AAAAAAAAAAAIT5aGdWo6qlb2a4GACZOR"
			+ "L1Dt7Wf4qCRYAIjZcPv66vhp/3UUZAAAABaLd/3DZvhpX2we8AFZWIHXy8ms6zvaTAAAsmwgzVXQ1VVt1RbcAAAA"
			+ "AAAAAAAhPloZ1ajqqVvZrgYAJk5EvUO3tZ/ioJFgAiNlw+/rq+Gn/dRRkAAAAFot3/cNm+GlfbB7wAVlYgdfLyaz"
			+ "rO9pMAACybCDNVdDVVW3VFtwAAAAAAAAAACE+WhnVqOqpW9muBgAmTkS9Q7e1n+KgkWACI2XD7+ur4af91FGQAAA"
			+ "AWi3f8AcNm+GlfbB7wAVlYgdfLyazrO9pMAACybCDNVdDVVW3VFtwAAAAAAAAAACE+WhnVqOqpW9muBgAmTkS9Q7"
			+ "e1n+KgkWACI2XD7+ur4af8AdRRkAATl6MeH/Za+1w4Tox4f9lr7XDhOjHh/2WvtcOE6MeH/AGWvtcOE6MeH/Za+1"
			+ "w4Tox4f9lr7XDhOjHh/2WvtcOF2qqSKFVqsmryuX9uVQoy6PLHljyQhyQfUAHGrVycbi2palcr9ahav8itTqc+Z+"
			+ "mtQhD9VKlGlHkh+n+uWLy9GPD/stfa4cJ0Y8P8Astfa4cJ0Y8P+y19rhwnRjw/7LX2uHCdGPD/stfa4cJ0Y8P8As"
			+ "tfa4cLjV4ccb33Ft20LqWFGzvRNiT6dn1T9+rxpzP2pVKNCh+qlyw5Y8kIcseRj+k5iB22RskeI6TmIHbZGyR4jp"
			+ "OYgdtkbJHiOk5iB22RskeI6TmIHbZGyR4jpOYgdtkbJHiOk5iB22RskeI6TmIHbZGyR4jpOYgdtkbJHiOk5iB22R"
			+ "skeI6TmIHbZGyR4jpOYgdtkbJHiOk5iB22RskeJLHBu8dfvbhpYluWv+1/OrlCZSm/tUP00eWEynRhyQ/8AVGDcw"
			+ "ARZx0xxvfcrEu0rDsWNnfwavQk0qH71XjTpctKXRpR5Y8sP+YxaD0nMQO2yNkjxHScxA7bI2SPEdJzEDtsjZI8R0"
			+ "nMQO2yNkjxHScxA7bI2SPEdJzEDtsjZI8R0nMQO2yNkjxHScxA7bI2SPEdJzEDtsjZI8R0nMQO2yNkjxHScxA7bI"
			+ "2SPEdJzEDtsjZI8R0nMQO2yNkjxOlYcXUs3H+wpt67/AP70bWq8+lZ9D+BT/Zl/tUKNGnDlo8kfbyzKXt5extfRj"
			+ "w/7LX2uHCdGPD/stfa4cJ0Y8P8Astfa4cJ0Y8P+y19rhwnRjw/7LX2uHCdGPD/stfa4cLoWG+H9i4eWXWqhd6FZ/"
			+ "j1md+/T/kTf1x/V+mFH2R5IezkhBtoANCxKwqu5iLWqjWLxQrv7lToUpcr+PO/RDkpRhGPL7I8v9NN6MeH/AGWvt"
			+ "cOE6MeH/Za+1w4Tox4f9lr7XDhOjHh/2WvtcOE6MeH/AGWvtcOE6MeH/Za+1w4Tox4f9lr7XDhdvAAAAAABW1i/n"
			+ "VvfrWs72k1AAAABYRk1ZkLr/wCU3fTHTAAQIyrc99t/5Vbc0HIgAAAE2ci/NVXtazd1Kd7AAAAAAAAAAAAABW1i/"
			+ "nVvfrWs72k1AAAABYRk1ZkLr/5Td9MdMABAjKtz323/AJVbc0HIgAAAE2ci/NVXtazd1Kd7AAAAAAAAAAAAABW1i"
			+ "/nVvfrWs72k1AAAABYRk1ZkLr/5Td9MdMABAjKtz323/lVtzQciAAAATZyL81Ve1rN3Up3sAAAAAAAAAAAAAFbWL"
			+ "+dW9+tazvaTUAAAAFhGTVmQuv8A5Td9MdMABAjKtz323/lVtzQciAAAATZyL81Ve1rN3Up3sAAAAAAAAAAAAAFbW"
			+ "L+dW9+tazvaTUAAAAFhGTVmQuv/AJTd9MdMABAjKtz323/lVtzQciAAAATZyL81Ve1rN3Up3sAAAAAAAAAAAAAFb"
			+ "WL+dW9+tazvaTUAAAAFhGTVmQuv/lN30x0wAECMq3Pfbf8AlVtzQciAAAATZyL81Ve1rN3Up3sAAAAAAAAAAAAAF"
			+ "bWL+dW9+tazvaTUAAAAFhGTVmQuv/lN30x0wAECMq3Pfbf+VW3NByIAAABNnIvzVV7Ws3dSnewAAAAAAf/Z";

	private static MockedStatic<ImagenConverter> imagenConverter;
	
	@BeforeAll
	public static void ini() {
		imagenConverter = Mockito.mockStatic(ImagenConverter.class);
	}
	
	@AfterAll
	public static void fin() {
		imagenConverter.close();
	}
	
	@Test
	void findAllTest() {				
		List<ImagenDocument> imagenesDocument = new ArrayList<>();
		imagenesDocument.add(imagenDocument);
		Mockito.when(imagenRepository.findAll()).thenReturn(imagenesDocument);

		Assertions.assertEquals(imagenesDocument.size(), imagenServiceImpl.findAll().size());
	}
	
	@Test
	void getAllPagesTest() {
		List<ImagenDocument> imagenesDocument = new ArrayList<>();
		imagenesDocument.add(imagenDocument);
		Pageable paging = PageRequest.of(1, 1, Sort.by("titulo"));
		
		Page<ImagenDocument> imagenesPageDocument = new PageImpl<>(imagenesDocument, PageRequest.of(1, 1), 400L);
		Mockito.when(imagenRepository.findAll(paging)).thenReturn(imagenesPageDocument);

		Assertions.assertEquals(imagenesDocument.size(), imagenServiceImpl.getAllPages(1, 1, "titulo").getSize());
	}
	
	@Test
	void findOneTestWhenImagenDtoIsNotPresent() {		
		Mockito.when(imagenRepository.findById(ID)).thenReturn(Optional.ofNullable(null));

		Assertions.assertThrows(NoEncontrado.class, () -> imagenServiceImpl.findOne(ID));
	}
	
	@Test
	void findOneTestWhenImagenDtoIsPresent() {		
		Mockito.when(imagenRepository.findById(ID)).thenReturn(Optional.of(imagenDocument));
		imagenConverter.when(() -> ImagenConverter.convertImagenDocumentToOImagenDto(imagenDocument)).thenReturn(oImagenDto);
		Mockito.when(oImagenDto.getId()).thenReturn(ID);

		Assertions.assertEquals(ID, imagenServiceImpl.findOne(ID).getId());
	}

	@Test
	void saveTestWhenIdIsPresent() {
		Mockito.when(iImagenDto.getId()).thenReturn(ID);
		
		Assertions.assertThrows(Encontrado.class, () -> imagenServiceImpl.save(iImagenDto));
	}
	
	@Test
	void saveTestWhenImagenDtoIsNotPresent() {
		Mockito.when(iImagenDto.getId()).thenReturn("");
		imagenConverter.when(() -> ImagenConverter.convertIImagenDtoToImagenDocument(iImagenDto)).thenReturn(imagenDocument);

		Mockito.when(imagenRepository.save(imagenDocument)).thenReturn(imagenDocument);
		imagenConverter.when(() -> ImagenConverter.convertImagenDocumentToOImagenDto(imagenDocument)).thenReturn(oImagenDto);
		
		Assertions.assertEquals(ImagenConverter.convertImagenDocumentToOImagenDto(imagenDocument).getId(), imagenServiceImpl.save(iImagenDto).getId());
	}
	
	@Test
	void saveTestWhenImagenDtoIsPresent() {		
		Mockito.when(iImagenDto.getId()).thenReturn(ID);

		Assertions.assertThrows(Encontrado.class, () -> imagenServiceImpl.save(iImagenDto));
	}
	
	@Test
	void saveFileTest() throws IOException {
		MultipartFile imagenfile = Mockito.mock(MultipartFile.class);
		Mockito.when(imagenfile.getOriginalFilename()).thenReturn(TITULO);
		Mockito.when(imagenfile.getContentType()).thenReturn(MIMETYPE);
		Mockito.when(imagenfile.getBytes()).thenReturn(IMAGEN64.getBytes());
		
		Mockito.when(imagenRepository.save(imagenDocument)).thenReturn(imagenDocument);
		imagenConverter.when(() -> ImagenConverter.convertImagenDocumentToOImagenDto(imagenDocument)).thenReturn(oImagenDto);
		Mockito.when(oImagenDto.getId()).thenReturn(ID);
		
		Assertions.assertEquals(ID, imagenServiceImpl.saveFile(imagenfile).getId());
	}
	
	@Test
	void updateTestWhenImagenDtoIsNotPresent() {
		Mockito.when(iImagenDto.getId()).thenReturn(ID);

		Mockito.when(imagenRepository.findById(ID)).thenReturn(Optional.ofNullable(null));
		
		Assertions.assertThrows(NoEncontrado.class, () -> imagenServiceImpl.update(iImagenDto));
	}
	
	@Test
	void updateTestWhenImagenDtoIsPresent() {
		Mockito.when(iImagenDto.getId()).thenReturn(ID);
		Mockito.when(oImagenDto.getId()).thenReturn(ID);
		imagenConverter.when(() -> ImagenConverter.convertIImagenDtoToImagenDocument(iImagenDto)).thenReturn(imagenDocument);

		Mockito.when(imagenRepository.findById(ID)).thenReturn(Optional.of(imagenDocument));
		imagenConverter.when(() -> ImagenConverter.convertImagenDocumentToOImagenDto(imagenDocument)).thenReturn(oImagenDto);
		Mockito.when(imagenRepository.save(imagenDocument)).thenReturn(imagenDocument);
		
		Assertions.assertEquals(ID, imagenServiceImpl.update(iImagenDto).getId());
	}
	
	@Test
	void updateFileTestWhenImagenDtoIsNotPresent() throws IOException {
		Mockito.when(imagenRepository.findById(ID)).thenReturn(Optional.ofNullable(null));
		
		MultipartFile imagenfile = Mockito.mock(MultipartFile.class);
		
		Assertions.assertThrows(NoEncontrado.class, () -> imagenServiceImpl.updateFile(ID, imagenfile));
	}
	
	@Test
	void updateFileTestWhenImagenDtoIsPresent() throws IOException {
		Mockito.when(imagenRepository.findById(ID)).thenReturn(Optional.of(imagenDocument));
		
		MultipartFile imagenfile = Mockito.mock(MultipartFile.class);
		Mockito.when(imagenfile.getOriginalFilename()).thenReturn(TITULO);
		Mockito.when(imagenfile.getContentType()).thenReturn(MIMETYPE);
		Mockito.when(imagenfile.getBytes()).thenReturn(IMAGEN64.getBytes());
		
		Mockito.when(imagenRepository.save(imagenDocument)).thenReturn(imagenDocument);
		imagenConverter.when(() -> ImagenConverter.convertImagenDocumentToOImagenDto(imagenDocument)).thenReturn(oImagenDto);
		Mockito.when(oImagenDto.getId()).thenReturn(ID);
		
		Assertions.assertEquals(ID, imagenServiceImpl.updateFile(ID, imagenfile).getId());
	}
	
	@Test
	void deleteTestWhenImagenDtoIsNotPresent() {
		Mockito.when(imagenRepository.findById(ID)).thenReturn(Optional.ofNullable(null));
		
		Assertions.assertThrows(NoEncontrado.class, () -> imagenServiceImpl.deleteOne(ID));
	}
	
	@Test
	void deleteTestWhenImagenDtoIsPresent() {
		Mockito.when(imagenRepository.findById(ID)).thenReturn(Optional.of(imagenDocument));
		imagenConverter.when(() -> ImagenConverter.convertImagenDocumentToOImagenDto(imagenDocument)).thenReturn(oImagenDto);
		Mockito.when(oImagenDto.getId()).thenReturn(ID);
		
		Assertions.assertEquals(ID, imagenServiceImpl.deleteOne(ID).getId());
	}

}
